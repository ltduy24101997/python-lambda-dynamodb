import os
DYNAMO_DB_ACCESS_KEY = ''
DYNAMO_DB_SECRET_ACCESS_KEY = ''
DYNAMO_DB_REGION = 'ap-south-1'
DYNAMO_DB_MAX_CONNECTION = 2
DYNAMO_DB_URL = 'http://localhost:6000' if not DYNAMO_DB_REGION else f"https://dynamodb.{DYNAMO_DB_REGION}.amazonaws.com"
SERVICE_NAME = "python-demo"
LOG_DEBUG = False
JWT_SECRET = "asdag'12;l,mo[1123"
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            "datefmt": "%d/%b/%Y-%H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO"
        },
    },
    "loggers": {
        "": {
            "handlers": [
                "console"
            ],
            "level": "INFO"
        }
    }
}
try:
    from config.local_settings import *
except Exception as e:
    print(e)
