# Flask API with Lambda and DynamoDB demo

This project's built to address coding challenge about how to build Rest API with Python, AWS Lambda, AWS DynamoDB.

This project use Python Flask to build API server and Serverless Framework to deploy my API server to AWS Lambda.

This project use GitlabCI for CI/CD run unit test and deploy my API server to AWS Lambda.

For development environment, this project use docker to run local DynamoDB container 


## Requirement

### Develop requirement

Python version: 3.8

docker-compose must be available

## Dev step

step 1: Clone project and move current directory to project path

Step 2: Install virtual environment

Step 3: Install requirement lib by `pip install -r requirements.txt`

Step 4: Create docker network by `docker network create docNet`

Step 5: Start local db by `docker-compose -f local/dynamodb/docker-compose.yml up -d`

Step 6: Create `config/local_settings.py` to override config value (`DYNAMO_DB_URL` must be set "http://localhost:6000" to use db on local)

Step 7: Export environment variable 
```
export FLASK_RUN_PORT=8000
export FLASK_APP=applications
export FLASK_DEBUG=True
```

Step 8: Run project with command `flask run`

## API collection
You see details of apis in [Sample collection](https://api.postman.com/collections/20895047-40c24646-2bb7-4c6f-89b9-cc5fe58ca26e?access_key=PMAT-01H2K665HHHMZNHKES0737J42E)
Add variable `host=https://2y213s27fd.execute-api.ap-south-1.amazonaws.com` to try api in demo server 
