import datetime
import logging
import sys
import uuid

from flask import jsonify
from flask import request

from applications.commons.exception import APIBreakException, APIWarningException
from applications.users.token import AuthenticationToken
from config import settings


class LogMessage(object):
    def __init__(self, logger=None, func_name="", user="", client_id="", trace_id=None, service_name=None):
        self._logger = self.get_logger(logger)
        self.func_name = func_name
        self.user = user
        self.client_id = client_id
        self.tracer = None
        self.span = None
        self.trace_id = uuid.uuid1().hex if not trace_id else trace_id
        self.service_name = service_name if service_name else settings.SERVICE_NAME

    def set_user_log(self, username):
        """
        :param username: this username will be show in each log line.
        :return:
        """
        self.user = username

    @property
    def pub_logger(self):
        """
        :return: logger object
        """
        return self._logger

    @staticmethod
    def get_logger(logger=None):
        """
        :param logger: logger name
        :return: logger object
        """
        if isinstance(logger, (logging.Logger, logging.RootLogger)):
            return logger
        else:
            return logging.getLogger("" if not logger else logger)

    @staticmethod
    def logs_message(level, message, logger_obj):
        """
        :param level: level to log
        :param message: message to log
        :param logger_obj: log adapter
        :return:
        """
        level = level.upper()
        logger_level = {
            'CRITICAL': 50,
            'ERROR': 40,
            'WARNING': 30,
            'INFO': 20,
            'DEBUG': 10,
            'NOTSET': 0
        }
        logger_obj.log(logger_level.get(level), message)

    def log(self, lv, message, func_name="", ):
        """
        :param lv: level to log
        :param message: message to log
        :param func_name: function name where we wrote log line
        :return:
        """
        log_message = {
            'func_name': self.func_name if not func_name else func_name,
            'message': message,
            'user': self.user,
            "traceback": "trace-id-{}".format(self.trace_id),
            "time": f"{datetime.datetime.now()}"
        }

        my_adapter = logging.LoggerAdapter(self._logger,
                                           {
                                               'func_name': self.func_name if not func_name else func_name,
                                               'user': self.user,
                                               "traceback": "trace-id-{}".format(self.trace_id),
                                               "time": f"{datetime.datetime.now()}"
                                           })

        # print(log_message)
        self.logs_message(lv, log_message, my_adapter)

    def info(self, message, func_name=""):
        """
        :param message: message to log
        :param func_name: function name where we wrote log line
        :return:
        """
        self.log("info", message, func_name)

    def error(self, message, func_name=""):
        """
        :param message: message to log
        :param func_name: function name where we wrote log line
        :return:
        """
        self.log("error", message, func_name)

    def warning(self, message, func_name=""):
        """
        :param message: message to log
        :param func_name: function name where we wrote log line
        :return:
        """
        self.log("warning", message, func_name)

    def debug(self, message, func_name=""):
        """
        :param message: message to log
        :param func_name: function name where we wrote log line
        :return:
        """
        if settings.LOG_DEBUG:
            self.log("info", message, func_name)

    @classmethod
    def init_log(cls, func_name, logger=None, user="", client_id="", service_name=""):
        """
        :param func_name: function name where we wrote log line
        :param logger: logger object or logger name.
        :param user: username to log
        :param client_id: client_id to identify who call api
        :param service_name: service name to identify what service logged
        :return:
        """
        if not logger:
            init_service_name = service_name if service_name else settings.SERVICE_NAME
            log_message = cls(func_name=func_name, user=user, service_name=init_service_name)
        elif isinstance(logger, cls):
            user = logger.user
            init_service_name = service_name if service_name else logger.service_name
            log_message = cls(func_name=func_name, logger=logger.pub_logger, user=user, client_id=logger.client_id, trace_id=logger.trace_id, service_name=init_service_name)
        elif isinstance(logger, MixingLog):
            user = logger.default.user
            init_service_name = service_name if service_name else logger.default.service_name
            log_message = cls(func_name=func_name, logger=logger.default.pub_logger, user=user, client_id=logger.client_id, trace_id=logger.default.trace_id, service_name=init_service_name)
        else:
            init_service_name = service_name if service_name else settings.SERVICE_NAME
            log_message = cls(func_name=func_name, logger=logger, user=user, client_id=client_id, service_name=init_service_name)
        return log_message


class MixingLog(object):
    def __init__(self, func_name="", user=None, init_span=True, _parent_logger=None, client_id="", service_name=None):
        self.func_name = func_name
        self.user = user
        self.default = None
        self.client_id = client_id
        self._parent_logger = _parent_logger
        self.span = None
        self.init_span = init_span
        self.service_name = service_name if service_name else settings.SERVICE_NAME
        self.init_log_base_settings()

    @property
    def trace_id(self):
        return self.default.trace_id if self.default else uuid.uuid1().hex

    def set_user_log(self, username):
        self.user = username
        if self.default:
            self.default.set_user_log(username)

    def init_log_base_settings(self):
        if self.init_span:
            if isinstance(self._parent_logger, (LogMessage, MixingLog)):
                self.user = self._parent_logger.user
                self.client_id = self._parent_logger.client_id
        self.default = LogMessage(logger="", func_name=self.func_name, client_id=self.client_id, user=self.user, service_name=self.service_name)
        self.span = self.default.span
        for i in settings.LOGGING["loggers"].keys():
            if i:
                setattr(self, i, LogMessage(logger=i, func_name=self.func_name, user=self.user, client_id=self.client_id, service_name=self.service_name))

    def set_component(self, value):
        return self.default.set_component(value)

    def info(self, message, func_name=""):
        self.default.info(message, func_name)

    def error(self, message, func_name=""):
        self.default.error(message, func_name)

    def warning(self, message, func_name=""):
        self.default.warning(message, func_name)

    def log(self, lv, message, func_name=""):
        self.default.log(lv, message, func_name)

    def debug(self, message, func_name=""):
        self.default.debug(message, func_name)


class APIResponse(object):
    def __init__(self, **kwargs):
        self.code_status = 0
        self.message = ""
        self.errors = {}
        self.data_resp = {}
        self.kwargs = {}
        self.trace = ""

    def check_message(self, message):
        self.message = self.message if self.message else message

    def check_code(self, code_status):
        self.code_status = self.code_status if self.code_status else code_status

    def add_errors(self, error):
        if isinstance(error, list):
            self.errors.update(error)
        else:
            self.errors.update(error)

    def get_code(self, code=1200):
        return self.code_status if self.code_status else code

    def make_format(self):
        if self.errors:
            code_status = self.get_code(1400)
            message = self.message
            result = False
        else:
            code_status = self.get_code(1200)
            message = self.message if self.message else 'Success'
            result = True
        return dict(result=result, message=str(message), status_code=code_status, request_id=str(self.trace), data=self.data_resp, error=self.errors, **self.kwargs)


def log_traceback(trac_back, e, _logger, log_type="info"):
    _logger.log(log_type, "{} - {}".format(e.__class__.__name__, str(e)))
    for i in range(8):
        if not i:
            continue
        if not trac_back:
            break
        if log_type == "info":
            _logger.log(log_type, 'Trace back exception at func {} - in line {}'.format(trac_back.tb_frame.f_code.co_name, trac_back.tb_lineno))
        else:
            _logger.log(log_type, 'Trace back exception at func {} - in line {}'.format(trac_back.tb_frame.f_code.co_name, trac_back.tb_lineno))
        trac_back = trac_back.tb_next


def trace_api(specific_logger="", validator=None, service_name=None, class_response=APIResponse):
    """
    :param specific_logger: Logger specific for api
    :param validator: Validator class for check input data
    :param service_name: Service name for log
    :param class_response: Response class to
    :return:
    """

    def decorator(func):
        """
        :param func: api function using this decorator
        :return: inner func
        """

        def inner(**kwargs):
            """
            :param kwargs: key word arguments
            :return: json payload to response to client
            """
            _response = class_response(api_func=func.__name__)
            kwargs.update(_response=_response)
            func_name = f"{func.__name__}"
            if specific_logger == "":
                _logger = LogMessage.init_log(func_name, service_name=service_name)
            elif specific_logger == "mix":
                _logger = MixingLog(func_name=func_name, service_name=service_name)
            else:
                _logger = LogMessage.init_log(func_name, service_name=service_name)
            kwargs.update(request=request, data_ser=None, logger=_logger)
            _response.trace = _logger.trace_id
            _logger.debug("<-------START------->")
            _logger.debug(f'Input Query string {request.args}')
            if request.method == "POST":
                _logger.debug(f'Input json request {request.json}')
                if validator:
                    data_ser = validator(data=request.json)
                    if not data_ser.is_valid():
                        _response.check_message('Data input is invalid.')
                        _response.check_code(1400)
                        _response.add_errors(data_ser.errors)
                        _logger.info('Error: {}'.format(data_ser.errors))
                        return jsonify(_response.make_format())
                    kwargs.update(data_ser=data_ser.valid_data)
            try:
                func(**kwargs)
            except Exception as e:
                if not isinstance(e, APIBreakException):
                    _response.check_message('Ops! Something were wrong.')
                    _response.check_code(1400)
                    _response.add_errors({"unknown_error": {
                        'field': "",
                        'message': 'An error occurred. Please try again later.' if not _response.message else _response.message
                    }})
                    log_traceback(trac_back=sys.exc_info()[-1], _logger=_logger, e=e, log_type="warning")
                else:
                    log_traceback(trac_back=sys.exc_info()[-1], _logger=_logger, e=e)
            if _response.errors:
                _logger.warning("<-------END------->")
            else:
                _logger.debug("<-------END------->")
            return jsonify(_response.make_format())

        inner.func_name = func.__name__
        inner.__name__ = func.__name__
        return inner

    return decorator


def trace_func(specific_logger="", service_name="", use_fn_as_service_name=False):
    def decorator(func):
        def inner(*args, **kwargs):
            func_name = func.__name__
            _parent_logger = kwargs.get("logger", "")
            init_service_name = service_name
            if isinstance(_parent_logger, LogMessage):
                init_service_name = service_name if service_name else _parent_logger.service_name
            if use_fn_as_service_name:
                init_service_name = f"{init_service_name}_{func_name}"
            if specific_logger == "":
                _logger = LogMessage.init_log(func_name, logger=_parent_logger, service_name=init_service_name)
            elif specific_logger == "mixin":
                _logger = MixingLog(func_name, _parent_logger=_parent_logger, service_name=init_service_name)
            else:
                _logger = LogMessage.init_log(func_name, logger=specific_logger, service_name=init_service_name)
            kwargs.update(logger=_logger)
            return func(*args, **kwargs)

        return inner

    return decorator


def auth_required():
    def decorator(func):
        def inner(**kwargs):
            request = kwargs.get("request")
            _logger = kwargs.get("logger")
            _response = kwargs.get('_response', APIResponse())
            if _logger:
                _logger.func_name = str(func.__name__)
            if not request:
                raise APIWarningException("Not found request")
            access_token = str(request.headers.get('access-token')).split(" ")[-1]
            if not access_token:
                _response.message = "Access token not provided"
                _response.code_status = 1401
                raise APIWarningException(_response.message)
            token = AuthenticationToken.auth(access_token)
            if not token:
                _response.message = "Unauthorized"
                _response.code_status = 1401
                raise APIWarningException(_response.message)
            kwargs.update(token=token)
            return func(**kwargs)

        inner.__name__ = func.__name__
        return inner

    return decorator
