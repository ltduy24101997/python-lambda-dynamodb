from applications.commons.exception import ValidationError


class Field(object):
    def __init__(self, max_length=None, null=None, max_number=None, min_number=None, required=True):
        self.name = ''
        self.max_length = max_length
        self.null = null
        self.max_number = max_number
        self.min_number = min_number
        self.required = required

    def check_max_length(self, value):
        if self.max_length and self.max_length < len(value):
            raise ValidationError(f"Field {self.name} must be less than {self.max_length}")

    def check_null(self, value):
        if value is None:
            raise ValidationError(f"Field {self.name} must be not null")

    def check_dict(self, value):
        if not isinstance(value, dict):
            raise ValidationError(f"Field {self.name} must be not null")
        return dict(value)

    def check_list(self, value):
        if not isinstance(value, list):
            raise ValidationError(f"Field {self.name} must be not null")
        return list(value)

    def check_max_number(self, value):
        if self.max_number and self.max_number < value:
            raise ValidationError(f"Field {self.name} must be more than {self.max_number}")

    def check_min_number(self, value):
        if self.min_number and self.min_number > value:
            raise ValidationError(f"Field {self.name} must be more than {self.min_number}")

    def check_int(self, value):
        try:
            return int(value)
        except Exception as e:
            raise ValidationError(f"Field {self.name} must be integer")

    def check_float(self, value):
        try:
            float(value)
        except Exception as e:
            raise ValidationError(f"Field {self.name} must be float")

    def check_boolean(self, value):
        try:
            bool(value)
        except Exception as e:
            raise ValidationError(f"Field {self.name} must be boolean")

    def check_string(self, value):
        if value is None:
            raise ValidationError(f"Field {self.name} must be boolean")
        return str(value)

    def clean(self, value):
        pass


class CharField(Field):
    def clean(self, value):
        self.check_null(value)
        self.check_max_length(value)
        return self.check_string(value)


class IntegerField(Field):

    def __init__(self, **kwargs):
        if kwargs.get("max_number"):
            if not isinstance(kwargs.get("max_number"), (int, float)):
                raise Exception("max_number must be int or float")
        if kwargs.get("min_number"):
            if not isinstance(kwargs.get("min_number"), (int, float)):
                raise Exception("max_number must be int or float")
        super(IntegerField, **kwargs).__init__(**kwargs)

    def clean(self, value):
        self.check_null(value)
        self.check_min_number(value)
        self.check_max_number(value)
        return self.check_int(value)


class FloatField(Field):

    def __init__(self, **kwargs):
        if kwargs.get("max_number"):
            if not isinstance(kwargs.get("max_number"), (int, float)):
                raise Exception("max_number must be int or float")
        if kwargs.get("min_number"):
            if not isinstance(kwargs.get("min_number"), (int, float)):
                raise Exception("max_number must be int or float")
        super(FloatField, **kwargs).__init__(**kwargs)

    def clean(self, value):
        self.check_null(value)
        self.check_min_number(value)
        self.check_max_number(value)
        return self.check_float(value)


class BooleanField(Field):

    def clean(self, value):
        self.check_null(value)
        return self.check_boolean(value)


class DictField(Field):

    def clean(self, value):
        self.check_null(value)
        return self.check_dict(value)


class ListField(Field):

    def clean(self, value):
        self.check_null(value)
        return self.check_list(value)


class Validators(object):

    def __init__(self, data):
        self.validator = self.get_fields()
        self.data = data
        self.valid_data = {}
        self.errors = {}
        self._is_valid = False

    def is_valid(self):
        invalid = False
        for key, field_validator in self.validator.items():
            if field_validator.required and self.data.get(key, 'not_found') == "not_found":
                self.errors.update({key: f"{key} must be required"})
                invalid = True
            try:
                clean_value = field_validator.clean(self.data.get(key))
                self.valid_data.update({key: clean_value})
            except Exception as e:
                invalid = True
                self.errors.update({key: str(e)})
        return not invalid

    @classmethod
    def get_fields(cls):
        validator = {}
        for attr, value in cls.__dict__.items():
            if isinstance(value, Field):
                value.name = attr
                validator.update({attr: value})
        return validator
