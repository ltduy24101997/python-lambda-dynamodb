import uuid

from pynamodb.models import Model
from pynamodb.util import attribute_value_to_json

from applications.commons.log_lib import trace_func


def id_generator():
    return uuid.uuid1().hex


class ModelBase(Model):
    @classmethod
    @trace_func()
    def first(cls, queryset, logger=None):
        """
        :param queryset: queryset after execute query to dynamoDB
        :param logger: logger object to log
        :return: first record for queryset or None
        """
        try:
            return queryset.next()
        except Exception as e:
            logger.error(e)
            return None

    @staticmethod
    def to_list(queryset):
        """
        Get all record of queryset and append it into list

        Args:
            queryset (_type_): queryset after execute query to dynamoDB
        """
        objects = []
        while True:
            try:
                next_item = queryset.next()
                objects.append(next_item)
            except StopIteration:
                break
        return objects

    @classmethod
    def get_all(cls):
        return cls.scan()

    def to_dict(self) -> dict:
        return {k: attribute_value_to_json(v) for k, v in self.serialize().items()}

    @classmethod
    def instance_list_to_dict(cls, instances):
        return [i.to_dict() for i in instances]
