from flask import Blueprint

from applications.notes.views import *

note_api = Blueprint('note', __name__)
note_api.add_url_rule('/create', view_func=api_note_create, methods=['POST'])
note_api.add_url_rule('/list', view_func=api_note_list, methods=['GET'])
note_api.add_url_rule('/all', view_func=api_note_all, methods=['GET'])
