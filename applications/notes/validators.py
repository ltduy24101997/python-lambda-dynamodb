from applications.commons import validators


class NoteCreateValidator(validators.Validators):
    note = validators.CharField(null=False)
