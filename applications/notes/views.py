from applications.commons.exception import APIWarningException
from applications.commons.log_lib import trace_api, auth_required, APIResponse
from applications.notes.models import Note
from applications.notes.validators import NoteCreateValidator
from applications.users.models import User
from applications.users.token import AuthenticationToken


@trace_api(validator=NoteCreateValidator)
@auth_required()
def api_note_create(_response: APIResponse, data_ser: dict, token: AuthenticationToken, **kwargs):
    note = data_ser.get("note")
    user_id = token.user_id
    note = Note(note=note, user_id=user_id)
    note.save()


@trace_api()
@auth_required()
def api_note_list(_response: APIResponse, data_ser: dict, token: AuthenticationToken, request, logger, **kwargs):
    username = request.args.get("username")
    limit = 10
    try:
        limit = int(request.args.get("limit"))
    except Exception as e:
        pass
    last_evaluated_key = request.args.get("last_evaluated_key")
    note_condition = {
        "hash_key": Note.DEFAULT_PK,
        "limit": limit
    }
    if last_evaluated_key:
        note_condition.update({
            "last_evaluated_key": {
                'note_id':
                    {'S': last_evaluated_key},
                'pk': {'S': 'default'}
            }
        })
    if not username:
        note_condition.update(dict(
            hash_key=Note.DEFAULT_PK,
            filter_condition=Note.user_id == token.user_id
        ))
    else:
        user = User.query(User.DEFAULT_PK, filter_condition=User.username == username)
        user = User.first(user, logger=logger)
        if not user:
            _response.message = f"User {username} does not exists"
            raise APIWarningException(_response.message)
        note_condition.update(dict(
            hash_key=Note.DEFAULT_PK,
            filter_condition=Note.user_id == user.user_id
        ))
    notes_queryset = Note.query(**note_condition)
    notes = Note.to_list(notes_queryset)
    _response.data_resp = {
        "notes": Note.instance_list_to_dict(notes),
    }
    return


@trace_api()
@auth_required()
def api_note_all(_response: APIResponse, data_ser: dict, token: AuthenticationToken, request, logger, **kwargs):
    limit = 10
    try:
        limit = int(request.args.get("limit"))
    except Exception:
        pass
    last_evaluated_key = request.args.get("last_evaluated_key")
    note_condition = {
        "hash_key": Note.DEFAULT_PK,
        "limit": limit
    }
    if last_evaluated_key:
        note_condition.update({
            "last_evaluated_key": {
                'note_id':
                    {'S': last_evaluated_key},
                'pk': {'S': 'default'}
            }
        })

    notes_queryset = Note.query(**note_condition)
    notes = Note.to_list(notes_queryset)
    _response.data_resp = {
        "notes": Note.instance_list_to_dict(notes)
    }
    return
