from flask import Blueprint

from applications.users.views import api_register, api_login, api_change_user_name, api_get_me

user_api = Blueprint('user', __name__)
user_api.add_url_rule('/register', view_func=api_register, methods=['POST'])
user_api.add_url_rule('/login', view_func=api_login, methods=['POST'])
user_api.add_url_rule('/change', view_func=api_change_user_name, methods=['POST'])
user_api.add_url_rule('/me', view_func=api_get_me, methods=['GET'])
