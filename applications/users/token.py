import datetime
import time

import jwt

from applications.commons.utils import datetime2timestamp
from config import settings


class AuthenticationToken(object):
    def __init__(self, user_id: str, expired_in: int):
        self.user_id = user_id
        self.expired_at = datetime.datetime.now() + datetime.timedelta(seconds=expired_in)

    @property
    def token(self):
        """
        :return: access-token after user login
        """
        payload = {
            "user_id": self.user_id,
            "expired_at": datetime2timestamp(self.expired_at)
        }
        return jwt.encode(payload, settings.JWT_SECRET, algorithm="HS256")

    @classmethod
    def auth(cls, token):
        """
        Function use for check access-token is valid or not
        :param token: access-token in header of request
        :return: AuthenticationToken object if token valid or None if token is invalid
        """
        ins = None
        try:
            payload = jwt.decode(token, settings.JWT_SECRET, algorithms="HS256")
            ins = cls(user_id=payload.get("user_id"), expired_in=payload.get("expired_at") - time.time())
        except Exception as e:
            print(e)
        return ins
