import datetime
from typing import Dict, Any

from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute

from applications.commons.models import ModelBase, id_generator
from config import settings


class User(ModelBase):
    DEFAULT_PK = "default"

    class Meta:
        table_name = 'UsersTable'
        read_capacity_units = 10
        write_capacity_units = 10
        max_pool_connections = 1
        host = settings.DYNAMO_DB_URL
        aws_access_key_id = settings.DYNAMO_DB_ACCESS_KEY
        aws_secret_access_key = settings.DYNAMO_DB_SECRET_ACCESS_KEY
        region = settings.DYNAMO_DB_REGION

    pk = UnicodeAttribute(default=DEFAULT_PK, hash_key=True)
    user_id = UnicodeAttribute(default=id_generator, range_key=True)
    create_at = UTCDateTimeAttribute(default=datetime.datetime.now)
    username = UnicodeAttribute()
    password = UnicodeAttribute()

    def save(self, **kwargs) -> Dict[str, Any]:
        return super(User, self).save(**kwargs)
