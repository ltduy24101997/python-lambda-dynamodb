import hashlib

from pynamodb.exceptions import PutError

from applications.commons.exception import APIWarningException
from applications.commons.log_lib import trace_api, APIResponse, auth_required
from applications.users.models import User
from applications.users.token import AuthenticationToken
from applications.users.validators import LoginValidators, ChangeUsernameValidators


@trace_api(validator=LoginValidators)
def api_register(_response: APIResponse, data_ser: dict, **kwargs):
    """
    API create new user account
    :param _response:
    :param data_ser:
    :param kwargs:
    :return:
    """
    _logger = kwargs["logger"]
    username = data_ser.get("username")
    password = data_ser.get("password")
    queryset = User.query(hash_key=User.DEFAULT_PK, filter_condition=User.username == username)
    user = User.first(queryset)
    if user:
        _response.message = f"User {username} exist"
        raise APIWarningException(_response.message)
    user = User(username=username, password=hashlib.md5(password.encode()).hexdigest())
    try:
        user.save()
        _response.message = "Create user successfully"
        _response.data_resp = {
            "user_id": user.user_id
        }
    except PutError as e:
        print(e)
        if e.cause_response_code == "ConditionalCheckFailedException":
            _response.message = f"User {username} exist"
        else:
            _response = str(e)
        raise APIWarningException(_response.message)


@trace_api(validator=LoginValidators)
def api_login(_response: APIResponse, data_ser: dict, **kwargs):
    """
    API perform login action and response access-token for user
    :param _response:
    :param data_ser:
    :param kwargs:
    :return:
    """
    username = data_ser.get("username")
    password = data_ser.get("password")
    hash_password = hashlib.md5(password.encode()).hexdigest()
    queryset = User.query(hash_key=User.DEFAULT_PK, filter_condition=User.username == username)
    user = User.first(queryset)
    if not user:
        _response.message = f"Invalid username or password"
        raise APIWarningException(_response.message)
    if user.password != hash_password:
        _response.message = f"Invalid username or password"
        raise APIWarningException(_response.message)
    _response.data_resp = {
        "access_token": AuthenticationToken(user_id=user.user_id, expired_in=360).token
    }
    return


@trace_api(validator=ChangeUsernameValidators)
@auth_required()
def api_change_user_name(_response: APIResponse, data_ser: dict, token: AuthenticationToken, **kwargs):
    """
    API perform change username action
    :param _response:
    :param data_ser:
    :param token:
    :param kwargs:
    :return:
    """
    username = data_ser.get("username")
    _logger = kwargs["logger"]
    queryset = User.query(hash_key=User.DEFAULT_PK, filter_condition=User.username == username)
    user_exist = User.first(queryset)
    if user_exist:
        _response.message = f"User {username} exist"
        raise APIWarningException(_response.message)
    queryset = User.query(hash_key=User.DEFAULT_PK, range_key_condition=User.user_id == token.user_id)
    user = User.first(queryset, logger=_logger)
    if not user:
        _response.message = "Unauthorized"
        _response.code_status = 1401
        raise APIWarningException(_response.message)

    try:
        user.username = username
        user.save()
        _response.message = "Update new user successfully"
        _response.data_resp = {
            "user_id": user.user_id
        }
    except PutError as e:
        if e.cause_response_code == "ConditionalCheckFailedException":
            _response.message = f"User {username} exist"
        else:
            _response = str(e)
        raise APIWarningException(_response.message)


@trace_api()
@auth_required()
def api_get_me(_response: APIResponse, token: AuthenticationToken, **kwargs):
    """
    API to get user information base on access-token in request header
    :param _response:
    :param data_ser:
    :param token:
    :param kwargs:
    :return:
    """
    queryset = User.query(hash_key=User.DEFAULT_PK, range_key_condition=User.user_id == token.user_id)
    user = User.first(queryset)
    if not user:
        _response.message = "Unauthorized"
        _response.code_status = 1401
        raise APIWarningException(_response.message)
    _response.data_resp = {
        "username": user.username,
        "user_id": user.user_id,
        "created_at": user.create_at
    }
