from applications.commons import validators


class LoginValidators(validators.Validators):
    username = validators.CharField(max_length=100, null=False)
    password = validators.CharField(max_length=100, null=False)


class ChangeUsernameValidators(validators.Validators):
    username = validators.CharField(max_length=100, null=False)
