from logging.config import dictConfig

from flask import Flask

from applications.notes.routes import note_api
from applications.users.routes import user_api
from config import settings


def create_app(config_class=settings):
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.register_blueprint(user_api, url_prefix="/api/v1/user")
    app.register_blueprint(note_api, url_prefix="/api/v1/note")
    dictConfig(settings.LOGGING)

    @app.route('/health', methods=['GET'])
    def heath_page():
        return 'Ok'

    return app


app = create_app()
