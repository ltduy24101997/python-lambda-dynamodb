import hashlib
import json

from applications import create_app
from applications.notes.models import Note
from applications.users.models import User
from applications.users.token import AuthenticationToken

app = create_app()
client = app.test_client()
try:
    Note.delete_table()
    User.delete_table()
except Exception as e:
    pass
Note.create_table()
User.create_table()
user1 = User(username="user1", password=hashlib.md5("password".encode()).hexdigest())
user2 = User(username="user2", password=hashlib.md5("password".encode()).hexdigest())
user1.save()
user2.save()

user1_token = AuthenticationToken(user_id=user1.user_id, expired_in=3600)
user2_token = AuthenticationToken(user_id=user2.user_id, expired_in=3600)
note1 = Note(user_id=user2.user_id, note="abc")
note1.save()

note2 = Note(user_id=user2.user_id, note="xyz")
note2.save()

note3 = Note(user_id=user2.user_id, note="ght")
note3.save()


def check_response_concept(json_response, is_success=True, status_code=None):
    assert isinstance(json_response, dict)
    if is_success and not status_code:
        assert json_response.get("status_code") == 1200
    elif not is_success and not status_code:
        assert json_response.get("status_code") == 1400
    else:
        assert json_response.get("status_code") == status_code

    assert isinstance(json_response.get("error"), dict)
    assert isinstance(json_response.get("data"), dict)
    if is_success:
        assert json_response.get("error") == dict()
        assert json_response.get("result") == True
    else:
        assert json_response.get("result") == False


def test_create_note_with_invalid_input():
    url = '/api/v1/note/create'
    payload = {
        "note": None
    }
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == 'Data input is invalid.'


def test_create_note_without_auth():
    url = '/api/v1/note/create'
    payload = {
        "note": "adasd"
    }
    header = {
        "Content-Type": "application/json",
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False, status_code=1401)
    assert response.json['message'] == 'Unauthorized'


def test_create_note_success():
    url = '/api/v1/note/create'
    payload = {
        "note": "qty"
    }
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'


def test_get_note_by_user():
    url = '/api/v1/note/list?username=user2'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 3


def test_get_own_note():
    url = '/api/v1/note/list'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 1


def test_get_note_paging():
    url = '/api/v1/note/list?username=user2&limit=2'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 2

    url = f'/api/v1/note/list?username=user2&limit=2&last_evaluated_key={response.json["data"]["notes"][-1]["note_id"]}'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 1


def test_get_all_note_paging():
    url = '/api/v1/note/all?limit=2'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 2

    url = f'/api/v1/note/all?limit=2&last_evaluated_key={response.json["data"]["notes"][-1]["note_id"]}'
    header = {
        "Content-Type": "application/json",
        "access-token": user1_token.token
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == 'Success'
    assert len(response.json["data"]["notes"]) == 2


def test_get_all_without_auth():
    url = '/api/v1/note/all?limit=2'
    header = {
        "Content-Type": "application/json"
    }
    response = client.get(url, headers=header)
    check_response_concept(response.json, is_success=False, status_code=1401)
    assert response.json['message'] == 'Unauthorized'
