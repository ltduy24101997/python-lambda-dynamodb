import hashlib
import json

from applications import create_app
from applications.users.models import User
from applications.users.token import AuthenticationToken

app = create_app()
client = app.test_client()

## init test env
try:
    User.delete_table()
except Exception as e:
    pass
User.create_table()
user1 = User(username="user1", password=hashlib.md5("password".encode()).hexdigest())
user2 = User(username="user2", password=hashlib.md5("password".encode()).hexdigest())
user1.save()
user2.save()
user1_token = AuthenticationToken(user_id=user1.user_id, expired_in=3600)
user2_token = AuthenticationToken(user_id=user2.user_id, expired_in=3600)


def check_response_concept(json_response, is_success=True, status_code=None):
    assert isinstance(json_response, dict)
    if is_success and not status_code:
        assert json_response.get("status_code") == 1200
    elif not is_success and not status_code:
        assert json_response.get("status_code") == 1400
    else:
        assert json_response.get("status_code") == status_code

    assert isinstance(json_response.get("error"), dict)
    assert isinstance(json_response.get("data"), dict)
    if is_success:
        assert json_response.get("error") == dict()
        assert json_response.get("result") == True
    else:
        assert json_response.get("result") == False


def test_api_register_success():
    url = '/api/v1/user/register'
    username = "admin"
    password = "admin"
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json)
    assert response.json['message'] == 'Create user successfully'


def test_api_register_user_exists():
    url = '/api/v1/user/register'
    username = "admin"
    password = "admin"
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'User {username} exist'


def test_api_register_with_invalid_password_input():
    url = '/api/v1/user/register'
    username = "admin"
    password = None
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Data input is invalid.'


def test_api_register_with_invalid_username_input():
    url = '/api/v1/user/register'
    username = None
    password = None
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Data input is invalid.'


def test_api_login_success():
    url = '/api/v1/user/login'
    username = "user2"
    password = "password"
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json)
    assert response.json['message'] == f'Success'
    assert isinstance(response.json["data"]["access_token"], str)


def test_api_login_with_invalid_user_name():
    url = '/api/v1/user/login'
    username = "asd"
    password = "admin"
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Invalid username or password'


def test_api_login_with_invalid_password():
    url = '/api/v1/user/login'
    username = "admin"
    password = "123"
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Invalid username or password'


def test_api_login_with_invalid_password_input():
    url = '/api/v1/user/login'
    username = "admin"
    password = None
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Data input is invalid.'


def test_api_login_with_invalid_username_input():
    url = '/api/v1/user/login'
    username = None
    password = None
    payload = {
        "username": username,
        "password": password
    }
    header = {
        "Content-Type": "application/json"
    }

    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Data input is invalid.'


def test_api_change_username_with_invalid_username_input():
    url = '/api/v1/user/change'
    new_username = None
    header = {
        "Content-Type": "application/json",

    }

    header.update({
        "access-token": user1_token.token
    })
    payload = {
        "username": new_username
    }
    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'Data input is invalid.'


def test_api_change_username_with_no_auth():
    url = '/api/v1/user/change'
    new_username = "admin"
    header = {
        "Content-Type": "application/json",

    }

    payload = {
        "username": new_username
    }
    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False, status_code=1401)
    assert response.json['message'] == f'Unauthorized'


def test_api_change_username_with_username_exists():
    url = '/api/v1/user/change'
    new_username = "user2"
    header = {
        "Content-Type": "application/json",

    }

    header.update({
        "access-token": user1_token.token
    })
    payload = {
        "username": new_username
    }
    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json, is_success=False)
    assert response.json['message'] == f'User {new_username} exist'


def test_api_change_username_success():
    url = '/api/v1/user/change'
    new_username = "user3"

    header = {
        "Content-Type": "application/json",

    }
    header.update({
        "access-token": user1_token.token
    })
    payload = {
        "username": new_username
    }
    response = client.post(url, headers=header, data=json.dumps(payload))
    check_response_concept(response.json)
    assert response.json['message'] == f'Update new user successfully'


def test_api_get_me_success():
    url = '/api/v1/user/me'
    header = {
        "Content-Type": "application/json",

    }

    header.update({
        "access-token": user1_token.token
    })
    response = client.get(url, headers=header)
    check_response_concept(response.json)
    assert response.json['message'] == f'Success'


def test_api_get_me_without_auth():
    url = '/api/v1/user/me'
    header = {
        "Content-Type": "application/json",

    }
    response = client.get(url, headers=header)
    check_response_concept(response.json, is_success=False, status_code=1401)
    assert response.json['message'] == f'Unauthorized'
