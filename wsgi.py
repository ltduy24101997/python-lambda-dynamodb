import sys
import logging

logging.basicConfig(stream=sys.stderr)
import applications

app = applications.create_app()
